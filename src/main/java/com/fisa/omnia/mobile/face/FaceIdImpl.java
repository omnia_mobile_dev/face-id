package com.fisa.omnia.mobile.face;

import com.fisa.omnia.mobile.FaceIdService;
import org.opencv.core.*;
import org.opencv.imgproc.Imgproc;
import org.opencv.objdetect.CascadeClassifier;
import java.awt.image.BufferedImage;
import java.util.Arrays;

import static org.opencv.imgcodecs.Imgcodecs.imread;

public class FaceIdImpl implements FaceIdService {

    static{ System.loadLibrary(Core.NATIVE_LIBRARY_NAME); }

    @Override
    public BufferedImage faceOf(String path){

        String classifierPath = System.getProperty("opencv.home");
        CascadeClassifier faceDetector = new CascadeClassifier(classifierPath+"/lbpcascades/lbpcascade_frontalface.xml");

        Mat image = imread(path);
        MatOfRect faceDetections = new MatOfRect();
        faceDetector.detectMultiScale(image,faceDetections);
        Rect rect  = faceDetections.toList().get(0);
        Rect rectCrop = new Rect(rect.x, rect.y, rect.width, rect.height);
        Mat image_roi = new Mat(image,rectCrop);

        Mat resizeimage = new Mat();
        Size sz = new Size(200,200);
        Imgproc.resize( image_roi, resizeimage, sz );

        /*Mat binarized = new Mat();
        Imgproc.adaptiveThreshold(image_roi, binarized, 255, ADAPTIVE_THRESH_GAUSSIAN_C,THRESH_BINARY,11,2);*/

        return this.bufferedImageOfMat(resizeimage);
    }

    private BufferedImage bufferedImageOfMat(Mat matrix) {
        int cols = matrix.cols();
        int rows = matrix.rows();
        int elemSize = (int)matrix.elemSize();
        byte[] data = new byte[cols * rows * elemSize];
        int type;
        matrix.get(0, 0, data);
        type = BufferedImage.TYPE_3BYTE_BGR;
        byte b;
        for(int i=0; i<data.length; i=i+3) {
            b = data[i];
            data[i] = data[i+2];
            data[i+2] = b;
        }
        BufferedImage image = new BufferedImage(cols, rows, type);
        image.getRaster().setDataElements(0, 0, cols, rows, data);


        /*Imgproc.cvtColor(rgba, rgba, Imgproc.COLOR_RGB2GRAY, 0);
        BufferedImage gray = new BufferedImage(rgba.width(), rgba.height(), BufferedImage.TYPE_BYTE_GRAY);
        byte[] data = ((DataBufferByte) gray.getRaster().getDataBuffer()).getData();
        rgba.get(0, 0, data);
        return gray;*/
        return image;
    }

    @Override
    public boolean match(String img_1, String img_2) {
        Mat mat_1 = imread(img_1);
        Mat mat_2 = imread(img_2);

        Mat hist_1 = new Mat();
        Mat hist_2 = new Mat();

        MatOfFloat ranges = new MatOfFloat(0f,256f);
        MatOfInt histSize = new MatOfInt(25);

        Imgproc.calcHist(Arrays.asList(mat_1), new MatOfInt(0),new Mat(), hist_1, histSize, ranges);
        Imgproc.calcHist(Arrays.asList(mat_2), new MatOfInt(0), new Mat(), hist_2, histSize, ranges);

        double res = Imgproc.compareHist(hist_1, hist_2, Imgproc.CV_COMP_CORREL);
        Double d = res * 100;
        return d > 65;
    }


    /*private Mat matOfBufferedImage(BufferedImage img){
        Mat mat = new Mat(img.getHeight(),img.getWidth(),CvType.CV_8UC3);
        int npixels = (int) (mat.total() * mat.elemSize());
        byte[] pixels = new byte[npixels];
        mat.put(0,0,pixels);

        Mat mat1 = new Mat(img.getHeight(),img.getWidth(),CvType.CV_8UC3);
        Imgproc.cvtColor(mat, mat1, Imgproc.COLOR_RGB2HSV);

        return mat1;
    }*/
}
