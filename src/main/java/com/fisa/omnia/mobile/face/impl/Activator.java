package com.fisa.omnia.mobile.face.impl;

import com.fisa.omnia.mobile.FaceIdService;
import com.fisa.omnia.mobile.face.FaceIdImpl;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceRegistration;

public class Activator implements BundleActivator {

    private ServiceRegistration serviceReference = null;

    @Override
    public void start(BundleContext bundleContext) throws Exception {
        serviceReference = bundleContext.registerService(FaceIdService.class.getName(), new FaceIdImpl(), null);
    }

    @Override
    public void stop(BundleContext bundleContext) throws Exception {
        System.out.println("Stopping Omnia FaceId bundle");
        if (serviceReference != null) {
            serviceReference.unregister();
            serviceReference = null;
        }
    }
}