package com.fisa.omnia.mobile;

import com.fisa.omnia.mobile.face.FaceIdImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.UUID;

public class AppTest{

    private File harley1;
    private File harley2;
    private File scarlet;
    private File angryDeveloper;

    private File harleyFace1;
    private File harleyFace2;
    private File scarletFace;

    @Before
    public void setUp() {

        ClassLoader classLoader = getClass().getClassLoader();
        this.angryDeveloper = new File(classLoader.getResource("input/angry_developer.png").getFile());
        this.harley1 = new File(classLoader.getResource("input/hayley1.jpg").getFile());
        this.harley2 = new File(classLoader.getResource("input/hayley2.jpg").getFile());
        this.scarlet = new File(classLoader.getResource("input/scarlet1.jpg").getFile());

        this.harleyFace1 = new File(classLoader.getResource("faces/hayley1.png").getFile());
        this.harleyFace2 = new File(classLoader.getResource("faces/hayley2.png").getFile());
        this.scarletFace = new File(classLoader.getResource("faces/scarlet.png").getFile());

        System.setProperty("opencv.home","/usr/local/Cellar/opencv/3.4.3_1/share/OpenCV/");

    }

    @Test
    public void faceRecognitionTest() throws IOException {
        FaceIdImpl faceId = new FaceIdImpl();
        BufferedImage faceOut = faceId.faceOf(this.angryDeveloper.getPath());
        Assert.assertNotNull(faceOut);

        String dir = System.getProperty("java.io.tmpdir") + File.separator + "faces" + File.separator;
        File outcome = new File(dir + UUID.randomUUID() + ".png");
        System.out.println(outcome.getPath());
        Files.createDirectories(Paths.get(dir));
        ImageIO.write(faceOut, "png", outcome);

        Assert.assertTrue(outcome.exists());
    }

    @Test
    public void similarImageTest() {
        FaceIdImpl faceId = new FaceIdImpl();
        //BufferedImage hayvey1 = ImageIO.read(harley1);
        //BufferedImage hayvey2 = ImageIO.read(harley2);
        Assert.assertTrue(faceId.match(harleyFace1.getPath(),harleyFace2.getPath()));
    }

    @Test
    public void diferentImageTest() {
        FaceIdImpl faceId = new FaceIdImpl();
        /*BufferedImage hayvey1 = ImageIO.read(harley1);
        BufferedImage scarlet1 = ImageIO.read(scarlet);*/
        Assert.assertTrue(!faceId.match(harleyFace1.getPath(),scarletFace.getPath()));
    }
}
